#!/bin/sh

momdir=$HOME/project/spam_vote_lotus

waitFor() {
  position=$1
  name_of_image=$2
  sec_waited=0

  diffreturn=1
  while [ "$diffreturn" -ne "0" ] && [ "$sec_waited" -lt "100" ]
  do
    sleep 0.5
    maim -g "$position" | diff - "$momdir/res/img/${name_of_image}.png" > /dev/null
    diffreturn=$?
  done

  [ "$sec_waited" -ge "100" ] && exit 2
}

pasteMail() {
  xdotool mousemove 325 941 click 1 #(dang ky tai khoan)
  xdotool mousemove 1645 539 click 1 #(copy)
  sleep 1

  xdotool mousemove 150 238 mousedown 1 #(mailbox)
  sleep 0.8
  xdotool mouseup 1 ##(mailbox)
  sleep 0.5
  xdotool mousemove 90 192 click 1 #(paste)
  sleep 0.5

  ## confirm
  xdotool mousemove 349 306 click 1 #(tiep tuc)

  waitFor '158x28+1853+693' receivedmail

  ## get code
  xdotool mousemove 2018 714 click 3 \
  mousemove 2062 974 click 1
  sleep 1
  xdotool mousemove 2293 689 click --repeat 2 1 \
    click --repeat 2 1 \
    click --repeat 2 1 \
    click --repeat 2 1 \
    key ctrl+c

  xdotool key F12 # inspect to copy code then F12 to close it

  ## paste
  xdotool mousemove 139 274 mousedown 1
  sleep 0.8
  xdotool mouseup 1 #(code box)
  sleep 0.5
  xdotool mousemove 86 226 click 1 #(paste)
  sleep 0.5
  xdotool mousemove 347 353 click 1 #(confirm)
  waitFor '110x90+304+160' codesuccess

  xdotool mousemove 371 402 click 1 #(last confirm)
  xdotool mousemove 2160 540 click 1 #(delete)
  sleep 1
}

pasteName() {
  ## get name
  rdn="$(echo $(shuf -n1 $momdir/res/ho; shuf -n1 $momdir/res/dem; shuf -n1 $momdir/res/ten;))"
  echo $rdn | xclip -sel c

  ## paste name
  xdotool mousemove 109 295 mousedown 1 #(name_box)
  sleep 0.9
  xdotool mouseup 1
  sleep 0.5
  xdotool mousemove 95 250 click 1 #(name_box_paste)
  sleep 0.5
  xdotool mousemove 340 370 click 1 #(name_box_finish)
  waitFor '100x30+310+935' notif1

  ## clean notif
  xdotool mousemove 361 946 click 1 mousemove 0 0 #(tiep theo)
  waitFor '100x30+310+935' notif2
  xdotool mousemove 361 777 click 1 #(tiep tuc)
  sleep 0.5
}

refreshLotus() {
  xdotool mousemove 539 1027 click 1 mousemove 0 0 #(open taskm)
  waitFor '400x30+150+1010' opened_taskm
  xdotool mousemove 150 550 click --repeat 2 --delay 500 5
  sleep 0.5
  xdotool mousemove 363 884 click 1
  # waitFor '200x30+250+930' opened_lotus
  waitFor '220x30+250+930' done
}

openLink() {
  xdotool mousemove 366 1031 click 1 #(home_button)
  waitFor '100x120+188+818' returnedHome
  xdotool mousemove 250 888 click 1 #(msg_button)
  waitFor '140x50+54+140' opened_msg
  xdotool mousemove 131 172 click 1 #(the msg)
  waitFor '100x60+50+935' openedMsg
  xdotool mousemove 316 845 click 1 #(link_button)
  waitFor '150x40+350+780' clicked_link
  xdotool mousemove 393 795 click 1 mousemove 0 0 #(link_open_button)
  waitFor '260x120+50+130' openpost_status
}

vote() {
  xdotool mousemove 69 663 click 1 mousemove 0 0 #(LIKE_button)
  waitFor '20x20+54+652' liked
  xdotool mousemove 77 719 click 1 mousemove 0 0 #(Share_button)
  waitFor '400x120+145+470' share1
  shuf -n1 $momdir/res/cmt | xclip -sel c
  xdotool mousemove 97 181 mousedown 1 #(retus_cmt)
  sleep 0.8
  xdotool mouseup 1
  sleep 0.5
  xdotool mousemove 87 132 click 1 #(retus_cmt_paste)
  sleep 0.5
  xdotool mousemove 626 76 click 1 mousemove 0 0 #(share2_button)
  waitFor '50x30+250+708' shared

  # hop ly
  #TODO: wait
  xdotool mousemove 275 892 click 1 #(tuyetvoi)
  waitFor '650x340+100+370' token1
  xdotool mousemove 352 685 click 1 #(dahieu)
  waitFor '670x100+100+880' token2
  xdotool mousemove 487 953 click 1 mousemove 0 0
  waitFor '500x70+120+700' token3
  xdotool mousemove 481 382 click 1 mousemove 0 0
  xdotool mousemove 513 744 click 1 mousemove 0 0
  waitFor '240x35+90+540' token4
  xdotool mousemove 346 661 click 1 mousemove 0 0
  waitFor '50x30+250+708' shared

  shuf -n1 $momdir/res/cmt | xclip -sel c
  xdotool mousemove 182 971 mousedown 1 #(cmt_button)
  sleep 0.8
  xdotool mouseup 1
  sleep 0.5
  xdotool mousemove 101 928 click 1 key BackSpace #(cmt_paste_button)
  sleep 1
  xdotool mousemove 651 976 click 1 #(cmt_send_button)

  ## wait for comment
  sleep 1
  diffreturn=1
  while [ "$diffreturn" -ne "0" ]
  do
    sleep 0.3
    xdotool mousemove 150 550 click 5
    maim -g 130x35+50+880 | diff - $momdir/res/img/cmt_done.png > /dev/null
    diffreturn=$?
  done
}

followPage() {
  xdotool mousemove 230 81 click 1 mousemove 0 0 #(open page)
  waitFor '200x30+250+250' opened_page
  xdotool mousemove 343 273 click 1 #(follow)
  waitFor '300x120+210+420' clicked_follow
  xdotool mousemove 374 637 click 1 #(confirm)
  waitFor '200x30+250+250' followed
  xdotool mousemove 62 89 click 1 #(exit page)
}

backHomePage() {
  xdotool mousemove 0 0
  diffreturn=1
  while [ "$diffreturn" -ne "0" ]
  do
    sleep 0.3
    xdotool mousemove 61 94 click 1 mousemove 0 0 #(exit post)
    sleep 0.5
    maim -g 60x25+60+70 | diff - $momdir/res/img/backhome_status.png > /dev/null
    diffreturn=$?
  done
}

delAcc() {
  ## logout
  # xdotool mousemove 61 94 click 1 #(exit post)
  # sleep 1
  backHomePage
  xdotool mousemove 605 982 click 1 #(personal)
  waitFor '30x30+640+64' opened_personal
  xdotool mousemove 665 72 click 1 #(setting)
  waitFor '220x15+280+68' opened_setting
  xdotool mousemove 356 762 click 5 #(swipe up)
  sleep 1
  xdotool mousemove 193 977 click 1 #(log out)

  ## wait for logout
  waitFor '160x68+283+114' logout_status

  ## delete account
  xdotool mousemove 224 373 click 1 mousemove 0 0 #(thoat1)
  waitFor '130x20+300+350' thoat2
  xdotool mousemove 604 255 click 1 mousemove 0 0 #(thoat2)
  waitFor '280x40+180+480' thoat3
  xdotool mousemove 524 553 click 1 mousemove 0 0 #(thoat3)
  waitFor '220x30+250+930' done
  sleep 1
}

## driver
refreshLotus
pasteMail
pasteName
openLink
# vote
# followPage
# delAcc
