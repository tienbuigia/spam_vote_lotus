#!/bin/sh

times=$1
momdir=$HOME/project/spam_vote_lotus

waitFor() {
  position=$1
  name_of_image=$2
  sec_waited=0

  diffreturn=1
  while [ "$diffreturn" -ne "0" ] && [ "$sec_waited" -lt "100" ]
  do
    sleep 0.3
    maim -g "$position" | diff - "$momdir/res/img/${name_of_image}.png" > /dev/null
    diffreturn=$?
  done

  [ "$sec_waited" -ge "100" ] && return 2
}

refreshLotus() {
  xdotool mousemove 539 1027 click 1 mousemove 0 0 #(open taskm)
  waitFor '400x30+150+1010' opened_taskm
  xdotool mousemove 150 550 click --repeat 2 --delay 500 5
  sleep 0.5
  xdotool mousemove 363 884 click 1
  # waitFor '200x30+250+930' opened_lotus
  waitFor '220x30+250+930' done
}

delAcc() {
  ## logout
  # xdotool mousemove 61 94 click 1 #(exit post)
  # sleep 1
  backHomePage
  xdotool mousemove 605 982 click 1 #(personal)
  waitFor '30x30+640+64' opened_personal
  xdotool mousemove 665 72 click 1 #(setting)
  waitFor '220x15+280+68' opened_setting
  xdotool mousemove 356 762 click 5 #(swipe up)
  sleep 1
  xdotool mousemove 193 977 click 1 #(log out)

  ## wait for logout
  waitFor '160x68+283+114' logout_status

  ## delete account
  xdotool mousemove 224 373 click 1 mousemove 0 0 #(thoat1)
  waitFor '130x20+300+350' thoat2
  xdotool mousemove 604 255 click 1 mousemove 0 0 #(thoat2)
  waitFor '280x40+180+480' thoat3
  xdotool mousemove 524 553 click 1 mousemove 0 0 #(thoat3)
  waitFor '220x30+250+930' done
  sleep 1
}

reset ()
{
  echo 'issue: waited too long!'
  refreshLotus
  delAcc
}

poweroffAndroid() {
  xdotool mousemove 712 72 mousedown 1 #(power)
  sleep 1.1
  xdotool mouseup 1
  sleep 0.5
  xdotool mousemove 622 364 click 1 #(poweroff)
  sleep 1m
}

runtime=$(($times * 55 / 3600 + 2))
sleep "${runtime}h" && echo "timeout: `date`, runtime: ${runtime}h" >> log && systemctl -i poweroff &

for t in $(seq 1 $times)
do
  ./src/main.sh
  [ "$?" = "2" ] && reset
  clear && echo "done $t times"
done

echo Finish!
poweroffAndroid
echo Bye, now poweroff!
systemctl -i poweroff
